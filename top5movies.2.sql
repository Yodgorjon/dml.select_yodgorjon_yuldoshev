--Solution 1
WITH top_rented_movies AS (
    SELECT 
        f.film_id,
        f.title,
        COUNT(*) AS rental_count,
        AVG(EXTRACT(YEAR FROM CURRENT_DATE) - f.release_year) AS expected_age
    FROM 
        film f
    INNER JOIN 
        inventory i ON f.film_id = i.film_id
    INNER JOIN 
        rental r ON i.inventory_id = r.inventory_id
    GROUP BY 
        f.film_id, f.title
    ORDER BY 
        rental_count DESC
    LIMIT 5
)
SELECT 
    t.title AS movie_title,
    t.rental_count,
    ROUND(t.expected_age) AS expected_age_of_audience
FROM 
    top_rented_movies t;

--Solution 2
SELECT 
    movie_title,
    rental_count,
    ROUND(expected_age) AS expected_age_of_audience
FROM 
    (
        SELECT 
            f.title AS movie_title,
            COUNT(*) AS rental_count,
            AVG(EXTRACT(YEAR FROM CURRENT_DATE) - f.release_year) AS expected_age
        FROM 
            film f
        INNER JOIN 
            inventory i ON f.film_id = i.film_id
        INNER JOIN 
            rental r ON i.inventory_id = r.inventory_id
        GROUP BY 
            f.title
        ORDER BY 
            rental_count DESC
        LIMIT 5
    ) AS top_rented_movies;
