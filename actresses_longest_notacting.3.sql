--Solution 1 
WITH actor_last_film AS (
    SELECT 
        a.actor_id,
        a.first_name || ' ' || a.last_name AS actor_name,
        MAX(f.release_year) AS last_film_year
    FROM 
        actor a
    LEFT JOIN 
        film_actor fa ON a.actor_id = fa.actor_id
    LEFT JOIN 
        film f ON fa.film_id = f.film_id
    GROUP BY 
        a.actor_id, actor_name
)
SELECT 
    actor_id,
    actor_name,
    CURRENT_DATE - last_film_year AS years_since_last_film
FROM 
    actor_last_film
ORDER BY 
    years_since_last_film DESC;

--Solution 2
SELECT 
    actor_id,
    actor_name,
    CURRENT_DATE - last_film_year AS years_since_last_film
FROM 
    (
        SELECT 
            a.actor_id,
            a.first_name || ' ' || a.last_name AS actor_name,
            MAX(f.release_year) AS last_film_year
        FROM 
            actor a
        LEFT JOIN 
            film_actor fa ON a.actor_id = fa.actor_id
        LEFT JOIN 
            film f ON fa.film_id = f.film_id
        GROUP BY 
            a.actor_id, actor_name
    ) AS actor_last_film
ORDER BY 
    years_since_last_film DESC;
