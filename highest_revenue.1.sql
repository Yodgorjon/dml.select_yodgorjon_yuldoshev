--Solution 1
WITH revenue_per_staff AS (
    SELECT 
        s.staff_id,
        s.first_name || ' ' || s.last_name AS staff_name,
        SUM(p.amount) AS total_revenue,
        EXTRACT(YEAR FROM p.payment_date) AS payment_year
    FROM 
        staff s
    INNER JOIN 
        payment p ON s.staff_id = p.staff_id
    WHERE 
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY 
        s.staff_id, s.first_name, s.last_name, EXTRACT(YEAR FROM p.payment_date)
)
SELECT 
    r.staff_id,
    r.staff_name,
    r.total_revenue
FROM 
    revenue_per_staff r
JOIN 
    (
        SELECT 
            staff_id,
            MAX(total_revenue) AS max_revenue
        FROM 
            revenue_per_staff
        GROUP BY 
            staff_id
    ) max_rev ON r.staff_id = max_rev.staff_id AND r.total_revenue = max_rev.max_revenue;

--Solution 2
SELECT 
    staff_id,
    staff_name,
    total_revenue
FROM 
    (
        SELECT 
            s.staff_id,
            s.first_name || ' ' || s.last_name AS staff_name,
            SUM(p.amount) AS total_revenue,
            RANK() OVER(PARTITION BY EXTRACT(YEAR FROM p.payment_date), s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
        FROM 
            staff s
        INNER JOIN 
            payment p ON s.staff_id = p.staff_id
        WHERE 
            EXTRACT(YEAR FROM p.payment_date) = 2017
        GROUP BY 
            s.staff_id, s.first_name, s.last_name, EXTRACT(YEAR FROM p.payment_date), s.store_id
    ) ranked_revenue
WHERE 
    revenue_rank = 1;
